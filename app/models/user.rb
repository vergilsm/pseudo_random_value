class User < ApplicationRecord
  serialize :character_array, Array

  validates :name, :character_array, presence: true

  has_many :posts

  before_validation do
    self.character_array = symbols_array
  end

  private

  def symbols_array
    inputs = (0..9).to_a | ('a'..'z').to_a | ('A'..'Z').to_a
    new_values = []

    7.times do
      new_values << inputs.shuffle
    end
    new_values.reverse
  end
end
