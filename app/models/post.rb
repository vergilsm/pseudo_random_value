class Post < ApplicationRecord
  belongs_to :user

  validates :post, presence: true
  validates :user_id, presence: :true

  after_create do
    self.update_attributes(token: character_generate.new_value(self.id))
  end

  private

  def character_generate
    PseudoRandomValue.new(self.user.character_array)
  end
end
