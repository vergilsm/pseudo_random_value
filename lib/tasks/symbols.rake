namespace :symbols do
  task get_an_array_of_characters: :environment do
    inputs = (0..9).to_a | ('a'..'z').to_a | ('A'..'Z').to_a
    new_values = []

    7.times do
      new_values << inputs.shuffle
    end

    File.open("config/symbols.yml", "w") { |f| YAML.dump(new_values.reverse, f) }
  end
end
